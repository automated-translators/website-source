+++
title = "Screenshots"

[[gallery_item]]
album = "Plasma"
image = "screenshots/plasma/main.png"
caption = "Main window"

[[gallery_item]]
album = "Plasma"
image = "screenshots/plasma/settings.png"
caption = "Settings"

[[gallery_item]]
album = "Plasma"
image = "screenshots/plasma/popup.png"
caption = "Pop-up window"

[[gallery_item]]
album = "Windows 10"
image = "screenshots/windows/main.png"
caption = "Main window"

[[gallery_item]]
album = "Windows 10"
image = "screenshots/windows/settings.png"
caption = "Settings"

[[gallery_item]]
album = "Windows 10"
image = "screenshots/windows/popup.png"
caption = "Pop-up window"

[[gallery_item]]
album = "Plasma Mobile"
image = "screenshots/plasma-mobile/main-portrait.png"
caption = "Main window"

[[gallery_item]]
album = "Plasma Mobile"
image = "screenshots/plasma-mobile/main-landscape.png"
caption = "Main window"

[[gallery_item]]
album = "Plasma Mobile"
image = "screenshots/plasma-mobile/settings-portrait.png"
caption = "Settings"

[[gallery_item]]
album = "Plasma Mobile"
image = "screenshots/plasma-mobile/settings-landscape.png"
caption = "Settings"
+++

### Plasma

{{< gallery album="Plasma" >}}

### Windows 10

{{< gallery album="Windows 10" >}}

### Plasma Mobile

{{< gallery album="Plasma Mobile" >}}

