+++
title = "Скриншоты"

[[gallery_item]]
album = "Plasma"
image = "screenshots/plasma/main.png"
caption = "Главное окно"

[[gallery_item]]
album = "Plasma"
image = "screenshots/plasma/settings.png"
caption = "Настройки"

[[gallery_item]]
album = "Plasma"
image = "screenshots/plasma/popup.png"
caption = "Всплывающее окно"

[[gallery_item]]
album = "Windows 10"
image = "screenshots/windows/main.png"
caption = "Главное окно"

[[gallery_item]]
album = "Windows 10"
image = "screenshots/windows/settings.png"
caption = "Настройки"

[[gallery_item]]
album = "Windows 10"
image = "screenshots/windows/popup.png"
caption = "Всплывающее окно"

[[gallery_item]]
album = "Plasma Mobile"
image = "screenshots/plasma-mobile/main-portrait.png"
caption = "Главное окно"

[[gallery_item]]
album = "Plasma Mobile"
image = "screenshots/plasma-mobile/main-landscape.png"
caption = "Главное окно"

[[gallery_item]]
album = "Plasma Mobile"
image = "screenshots/plasma-mobile/settings-portrait.png"
caption = "Настройки"

[[gallery_item]]
album = "Plasma Mobile"
image = "screenshots/plasma-mobile/settings-landscape.png"
caption = "Настройки"
+++

### Plasma

{{< gallery album="Plasma" >}}

### Windows 10

{{< gallery album="Windows 10" >}}

### Plasma Mobile

{{< gallery album="Plasma Mobile" >}}

