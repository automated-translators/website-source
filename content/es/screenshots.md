+++
title = "Capturas de pantalla"

[[gallery_item]]
album = "Plasma"
image = "screenshots/plasma/main.png"
caption = "Pantalla principal"

[[gallery_item]]
album = "Plasma"
image = "screenshots/plasma/settings.png"
caption = "Ajustes"

[[gallery_item]]
album = "Plasma"
image = "screenshots/plasma/popup.png"
caption = "Ventana emergente"

[[gallery_item]]
album = "Windows 10"
image = "screenshots/windows/main.png"
caption = "Pantalla principal"

[[gallery_item]]
album = "Windows 10"
image = "screenshots/windows/popup.png"
caption = "Ventana emergente"

[[gallery_item]]
album = "Plasma Mobile"
image = "screenshots/plasma-mobile/main-portrait.png"
caption = "Pantalla principal"

[[gallery_item]]
album = "Windows 10"
image = "screenshots/windows/settings.png"
caption = "Ajustes"

[[gallery_item]]
album = "Plasma Mobile"
image = "screenshots/plasma-mobile/main-landscape.png"
caption = "Pantalla principal"

[[gallery_item]]
album = "Plasma Mobile"
image = "screenshots/plasma-mobile/settings-portrait.png"
caption = "Ajustes"

[[gallery_item]]
album = "Plasma Mobile"
image = "screenshots/plasma-mobile/settings-landscape.png"
caption = "Ajustes"
+++

### Plasma

{{< gallery album="Plasma" >}}

### Windows 10

{{< gallery album="Windows 10" >}}

### Plasma Mobile

{{< gallery album="Plasma Mobile" >}}

